const express = require('express');
const app = express();

const fetch = require('node-fetch');
const config = require('./src/config.json');
const sampleTransactions = require('./src/1k-transactions.json');

const serveStatic = require('serve-static');

app.use(serveStatic('../casinim.gitlab.io/public'));

app.set('json spaces', 2);

app.get('/don', async (req, res) => {
  let transaction = getRandomTransaction();
  let hash = transaction.hash;
  let byteArray = new Uint8Array(hash.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
  res.json({
    "hash": transaction.hash,
    "win": byteArray[31] % 2 == 1
  });
});

app.get('/slot', (req, res) => {
  const transaction = getRandomTransaction();
  const hash = transaction.hash;
  const byteArray = new Uint8Array(hash.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
  let luckyNumbers = [-1, -1, -1];

  luckyNumbers[0] = getLuckyNumber(byteArray[29]);
  luckyNumbers[1] = getLuckyNumber(byteArray[30]);
  luckyNumbers[2] = getLuckyNumber(byteArray[31]);

  winType = getSlotWin(luckyNumbers);

  res.json({
    "hash": transaction.hash,
    "luckyNumbers": luckyNumbers,
    "win": winType
  });
});

// app.get('/slot', async (req, res) => {
//   const transaction = getRandomTransaction();
//   let hash = transaction.hash;
//   let byteArray = new Uint8Array(hash.match(/.{1,2}/g).map(byte => parseInt(byte, 16)));
//   let luckyNumbers = byteArray.map(number => number % 5);
//   let winNumbers = [luckyNumbers[29], luckyNumbers[30], luckyNumbers[31]];
//   let win = (
//     (winNumbers[0] == winNumbers[1] && winNumbers[1] == winNumbers[2]) || 
//     (winNumbers[1] == winNumbers[0] + 1 && winNumbers[2] == winNumbers[1] + 1) ||
//     (winNumbers[1] == winNumbers[0] - 1 && winNumbers[2] == winNumbers[1] - 1)
//   )
//   res.json({
//     "win": win,
//     "hash": transaction.hash,
//     "byteArray": byteArray.join(', '),
//     "luckyNumbers": luckyNumbers.join(', '),
//     "winNumbers": winNumbers.join(', '),
//   });
// });

app.get('/mockbob', async (req, res) => {
  let fetchRes = await fetch(`https://www.api.sparxdev.de/mockingbob/?value=${req.query.value}`);
  let json = await fetchRes.json();
  res.send(json);
});

getRandomTransaction = () => {
  const rand = Math.floor(Math.random() * sampleTransactions.length);
  const transaction = sampleTransactions[rand];
  return transaction;
}

getLuckyNumber = (number) => {
  if (number < 26) return 0;
  else if (number < 57) return 1;
  else if (number < 94) return 2;
  else if (number < 138) return 3;
  else if (number < 192) return 4;
  else return 5;
}

getSlotWin = (luckyNumbers) => {
  if (countInArray(luckyNumbers, 0) == 1) return '1x 0';
  else if (countInArray(luckyNumbers, 0) == 2) return '2x 0';
  else if (countInArray(luckyNumbers, 0) == 3) return '3x 0';
  else if (countInArray(luckyNumbers, 1) == 3) return '3x 1';
  else if (countInArray(luckyNumbers, 2) == 3) return '3x 2';
  else if (countInArray(luckyNumbers, 3) == 3) return '3x 3';
  else if (countInArray(luckyNumbers, 4) == 3) return '3x 4';
  else if (countInArray(luckyNumbers, 5) == 3) return '3x 5';
  else return 'no win';
}

countInArray = (array, what) => {
  let count = 0;
  for (let i = 0; i < array.length; i++) {
    if (array[i] == what) count++;
  }
  return count;
}

app.listen(26111);